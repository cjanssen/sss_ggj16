-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

function initIntro()
    Intro = {}

    Intro.logo = love.graphics.newImage("img/logo/sss03_trans.png")
    resetIntro()
end

function resetIntro()
    -- intro not repeated
    Intro.active = true
    Intro.opacity = 1
    Intro.logoOpacity = 1
    Intro.sel = 0
    Intro.disappear = false
    Intro.showCredits = false
end

function updateIntro(dt)
    if not Intro.active then
        return
    end
    if Intro.disappear then
        Intro.opacity = decreaseExponential(dt, Intro.opacity, 0.98)
        if Intro.opacity <= 0.1 then
            Intro.logoOpacity = decreaseExponential(dt, Intro.logoOpacity, 0.9)
        end

        if Intro.logoOpacity <= 0.01 then
            Intro.active = false
        end
    end

    if not fingers.active then
        local mpos = Vector(love.mouse.getX(), love.mouse.getY())
        mpos = (mpos - screenTopLeft) / screenAdjScale
        manageIntroMouse(mpos)
    end
end

function manageIntroMouse(mpos)
    Intro.sel = 0
    if mpos.y > screenSize.y * 0.8 then
        if mpos.x < screenSize.x * 0.2 then
            Intro.sel = 1
        elseif mpos.x > screenSize.x * 0.8 then
            Intro.sel = 2
        end
    end
end

function drawIntro()
    if not Intro.active then
        return
    end

    love.graphics.setColor(0,0,0, 255 * Intro.opacity)
    love.graphics.rectangle("fill", 0, 0, screenSize.x, screenSize.y)

    if Intro.showCredits then
        love.graphics.setFont(textFont)
        love.graphics.setColor(128,64,32)
        local creditText = "Made at the Global Game Jam 2010+6 in Berlin\n\nArt:\n  Alpha Rats + Kirill Krysov + Ruth Bosch\nGame Design:\n  Arisa Matzanke\nMusic:\n  AXL OTL\nCode:\n  Christiaan Janssen"
        love.graphics.print(creditText, screenSize.x*0.1, screenSize.y*0.1)

        love.graphics.setFont(buttonFont)
        if Intro.sel == 2 then
            love.graphics.setColor(255,128,128, 255 * Intro.opacity)
        else
            love.graphics.setColor(255,0,0, 255 * Intro.opacity)
        end
        love.graphics.print("Back", screenSize.x * 0.8, screenSize.y * 0.8)
    else

        love.graphics.setFont(buttonFont)
        if Intro.sel == 1 and not Intro.disappear then
            love.graphics.setColor(255,128,128, 255 * Intro.opacity)
        else
            love.graphics.setColor(255,0,0, 255 * Intro.opacity)
        end
        love.graphics.print("Play", screenSize.x * 0.1, screenSize.y * 0.8)

        if Intro.sel == 2 and not Intro.disappear then
            love.graphics.setColor(255,128,128, 255 * Intro.opacity)
        else
            love.graphics.setColor(255,0,0, 255 * Intro.opacity)
        end
        love.graphics.print("Credits", screenSize.x * 0.8, screenSize.y * 0.8)


        love.graphics.setColor(255,255,255, 255*Intro.logoOpacity)
        love.graphics.draw(Intro.logo, screenSize.x * 0.5, screenSize.y * 0.5, 0, 1, 1, Intro.logo:getWidth()*0.5, Intro.logo:getHeight()*0.5)
    end
end

function introClicked(fpos)
    if fpos then
        manageIntroMouse(fpos)
    end

    -- fingers
    if Intro.sel == 1 and not Intro.showCredits then
        Intro.disappear = true
    elseif Intro.sel == 2 and not Intro.disappear then
        Intro.showCredits = not Intro.showCredits
    end

    if fpos then Intro.sel = 0 end
end
