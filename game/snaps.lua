-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

function initSnaps()
    ShelfPositions = {
        Vector(250, 229),
        Vector(344, 238),
        Vector(466, 248),
        Vector(240, 356),
        Vector(325, 361),
        Vector(438, 362),
        Vector(236, 464),
        Vector(324, 466),
        Vector(436, 470),
        Vector(214, 566),
        Vector(321, 574),
        Vector(416, 580),
        Vector(1469, 217),
        Vector(1580, 213),
        Vector(1697, 201),
        Vector(1506, 334),
        Vector(1610, 329),
        Vector(1722, 317),
        Vector(1517, 428),
        Vector(1608, 414),
        Vector(1710, 405),
        Vector(1534, 560),
        Vector(1613, 548),
        Vector(1701, 526),
        Vector(750, 270),
        Vector(862, 264),
        Vector(1012, 269),
        Vector(1140, 262),
        }

    PentagramPositions = {
        Vector(966, 1064),
        Vector(1240, 904),
        Vector(1104, 729),
        Vector(804, 738),
        Vector(726, 918),
    }

    CentralPosition = Vector(968, 868)

    BurninatePosition = Vector(970, 505)
end

function addAllSnaps()
    Items.snaps = {}
    Items.pentaSnaps = {}

    for _,snapPos in ipairs(ShelfPositions) do
        addSnap(snapPos)
    end

    for _,snapPos in ipairs(PentagramPositions) do
        addSnap(snapPos, "pentagram")
    end

    addSnap(CentralPosition, "central")
    addSnap(BurninatePosition, "burninate")

    repositionItemsInShelves()
end


function addSnap(pos, isSpecial)
    local snap = {
        foot = pos,
        box = Items.snapBox + pos - Vector(Items.snapBox:width()*0.5, Items.snapBox:height()),
        isSpecial = isSpecial,
        }

    table.insert(Items.snaps, snap)
    if isSpecial == "pentagram" then
        table.insert(Items.pentaSnaps, snap)
    elseif isSpecial == "central" then
        Items.centralSnap = snap
    elseif isSpecial == "burninate" then
        Items.burninator = snap
    end
end
