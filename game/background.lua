-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

function initBackground()
    Background = {
        bg = love.graphics.newImage("img/bgs/bg.png"),
        fence = love.graphics.newImage("img/bgs/fence.png"),
        penta = love.graphics.newImage("img/bgs/penta.png"),
        candles = love.graphics.newImage("img/bgs/candles.png"),
    }

    local fireImg = love.graphics.newImage("img/bgs/fire.png")
    Background.fireBatch = love.graphics.newSpriteBatch(fireImg, 2)
    Background.fireQuads = {}
    local w = math.floor(fireImg:getWidth()/4)
    for i=1,4 do
        table.insert(Background.fireQuads, love.graphics.newQuad((i-1)*w, 0, w, fireImg:getHeight(), fireImg:getWidth(), fireImg:getHeight()))
    end

    Background.fireFrame = 1
    Background.fireDelay = 0.1
    Background.fireTime = 0.1


    Background.candlePos = {
        Vector(635, 518),
        Vector(983, 494),
        Vector(1088, 673),
        Vector(817, 845),
        Vector(537, 713),
    }
end

function updateBackground(dt)
    local dirty = false

    Background.fireTime = Background.fireTime + dt
    while Background.fireTime > Background.fireDelay do
        Background.fireTime = Background.fireTime - Background.fireDelay
        Background.fireFrame = (Background.fireFrame % #Background.fireQuads) + 1
        dirty = true
    end

    if dirty then
        Background.fireBatch:clear()
        Background.fireBatch:setColor(255,255,255)
        Background.fireBatch:add(Background.fireQuads[Background.fireFrame], 830, 340)

        Background.candleBatch:clear()
        local an = Items.animMap["Flame"]
        local frame = an.current
        for _,candlePos in ipairs(Background.candlePos) do
            local pic = Items.quads[an[frame]]
            Background.candleBatch:add(pic, candlePos.x, candlePos.y)
            frame = (frame % #an) + 1
        end
    end
end

function drawBackground()
    love.graphics.setColor(255,255,255)
    love.graphics.draw(Background.bg)
    drawBurn()
    love.graphics.draw(Background.fireBatch)
    love.graphics.draw(Background.fence)
    love.graphics.draw(Background.penta)
    love.graphics.draw(Background.candles)
    love.graphics.draw(Background.candleBatch)
end

function drawForeground()
end