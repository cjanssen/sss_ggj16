-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

function getLoveVer()
    if not love.getVersion then return 8 end
    local maj,minor,rel = love.getVersion()
    return minor
end

function cloneListRecur(tabl)
    local res = {}
    for _,elem in ipairs(tabl) do
        if type(elem) == "table" then
            table.insert(res, cloneListRecur(elem))
        else
            table.insert(res, elem)
        end
    end
    return res
end


function randomizedListCopy(list)
    local res = {}
    local cp = cloneListRecur(list)
    while #cp>0 do
        table.insert(res, table.remove(cp, math.random(#cp)))
    end
    return res
end

function findInList(list, cond)
    for _,elem in ipairs(list) do
        if cond(elem) then return elem end
    end
    return nil
end

function printCentered(txt, fnt, y)
    local fnt = fnt or font
    local w = fnt:getWidth(txt)
    love.graphics.setFont(fnt)
    love.graphics.print(txt, (screenSize.x - w)*0.5, y)
end

function isMouseDown()
    -- wrapper that replaces itself
    local func = nil
    if getLoveVer() < 10 then
        func = function() return love.mouse.isDown('l') end
    else
        func = function() return love.mouse.isDown(1) end
    end

    isMouseDown = func
    return func()
end


function setAdditiveMode()
    local func = nil
    if getLoveVer() < 10 then
        func = function() love.graphics.setBlendMode("additive") end
    else
        func = function() love.graphics.setBlendMode("add") end
    end
    setAdditiveMode = func
    func()
end



function increaseExponential(dt, var, amount)
    if var < 1 then
        var = 1 - (1 - var) * math.pow(amount, 60*dt)
        if var > 0.999 then
            var = 1
        end
    end
    return var
end

function decreaseExponential(dt, var, amount)
    if var > 0 then
        var = var * math.pow(amount, 60*dt)
        if var < 0.001 then
            var = 0
        end
    end
    return var
end

function getBatchImage(batch)
    local func = nil
    if getLoveVer() < 10 then
        func = function(batch) return batch:getImage() end
    else
        func = function(batch) return batch:getTexture() end
    end
    getBatchImage = func
    return func(batch)
end