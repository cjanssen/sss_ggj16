-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)


----------------------------------------------------
-- INITIALIZATIONS

function initRituals()
    Rituals = {
        active = false
    }

    Rituals.brightPenta = love.graphics.newImage("img/final/bright_penta.png")
    initGoatRitual()
    initDevilRitual()
    initSantaRitual()

    Rituals.hint = {}
end

function initGoatRitual()
    -- goat
    Rituals.goat = {}

    local img = love.graphics.newImage("img/final/goat.png")
    Rituals.goat.batch = love.graphics.newSpriteBatch(img, 1)
    Rituals.goat.quads = {
        love.graphics.newQuad(0,  0,320,320,960,320),
        love.graphics.newQuad(320,0,320,320,960,320),
        love.graphics.newQuad(640,0,320,320,960,320),
    }
    Rituals.goat.time = 0
    Rituals.goat.frame = 1
    Rituals.goat.delay = 0.2
    Rituals.goat.scale = 2
    Rituals.goat.ofs = Vector(-66.6, -100)

    Rituals.goat.sound = Sfx.goat
end

function initDevilRitual()
    -- devil
    Rituals.devil = {}

    local img = love.graphics.newImage("img/final/devil.png")
    Rituals.devil.batch = love.graphics.newSpriteBatch(img, 1)
    Rituals.devil.quads = {}
    for i=0,5 do
        table.insert(Rituals.devil.quads,
            love.graphics.newQuad(i*640,0, 640, 640, img:getWidth(), img:getHeight()))
    end

    Rituals.devil.time = 0
    Rituals.devil.frame = 1
    Rituals.devil.delay = 0.2
    Rituals.devil.ofs = Vector(0,-130)
    Rituals.devil.sound = Music.devil
end

function initSantaRitual()
    -- santa
    Rituals.santa = {}
    do
        local img = love.graphics.newImage("img/final/santa.png")
        Rituals.santa.batch = love.graphics.newSpriteBatch(img, 1)
        Rituals.santa.quads = {
            love.graphics.newQuad(0,  0,600,600,1200,600),
            love.graphics.newQuad(600,0,600,600,1200,600),
        }
        Rituals.santa.time = 0
        Rituals.santa.frame = 1
        Rituals.santa.delay = 0.2

        Rituals.santa.sound = Music.santa
    end

    do
        local img = love.graphics.newImage("img/final/snowflake.png")
        local ps = love.graphics.newParticleSystem(img,500)
        ps:setEmissionRate(10)
        ps:setSpeed(0, 100)
        ps:setLinearAcceleration(0,10)
        ps:setPosition(screenSize.x * 0.5, 0)
        ps:setParticleLifetime(15)
        ps:setDirection(math.pi / 2)
        ps:setAreaSpread("uniform", screenSize.x * 0.5,1)
        ps:setSizeVariation(1)

        ps:start()
        Rituals.santa.ps = ps
    end
end

-----------------------------------------------------------------------------------
-- RESET

function resetRituals()
    Rituals.active = false
    Rituals.time = 0
    Rituals.delay = 3
    Rituals.special = "normal"

    Rituals.santa.visible = false
    Rituals.goat.visible = false
    Rituals.devil.visible = false
    Rituals.overlayOp = 0
    Rituals.distortion = false
    Rituals.distMax = 0.15
    Rituals.distAmount = 0
    Rituals.distTime = 0

    Rituals.buttonOp = 0
end


------------------------------------------------------------------------
-- PREPARE
function combineRitual(combinationList)
    Rituals.elevate = {}
    for _,itemName in ipairs(combinationList) do
        table.insert(Rituals.elevate, findItem(itemName))
    end
end

function vibrateRitual(itemList)
    Rituals.vibrate = {}
    for _,itemName in ipairs(itemList) do
        table.insert(Rituals.vibrate, findItem(itemName))
    end
end

function appearRitual(newItem)
    Rituals.appear = newItem
    newItem.opacity = 0
end

function elevateAllObjs()
    Rituals.appear = {}

    local invoItems = {}
    for _,penta in ipairs(Items.pentaSnaps) do
        if penta.hasItem then
        table.insert(invoItems, penta.hasItem.name)
        end
    end
    vibrateRitual(invoItems)
    combineRitual(invoItems)
    playSound(Sfx.magic2)
end


--------------------------------------------------------------------------------
-- LAUNCH
function launchRitual()
    Rituals.active = true
    Rituals.time = 0
    Rituals.special = "normal"
    playSound(Sfx.magic)
end

function launchGoatRitual()
    Rituals.active = true
    Rituals.time = 0
    Rituals.special = Rituals.goat
    Rituals.goat.visible = false

    elevateAllObjs()
    playSound(Sfx.magic2)
end

function launchDevilRitual()
    Rituals.active = true
    Rituals.time = 0
    Rituals.special = Rituals.devil
    Rituals.devil.visible = false

    elevateAllObjs()
    playSound(Sfx.magic2)
end

function launchSantaRitual()
    Rituals.active = true
    Rituals.time = 0
    Rituals.special = Rituals.santa
    Rituals.santa.visible = false


    elevateAllObjs()
end

function launchHintRitual()
    Rituals.active = true
    Rituals.time = 0
    Rituals.special = Rituals.hint
    playSound(Sfx.laugh)
end

---------------------------------------------------------------------------------------------
-- UPDATE
function updateRituals(dt)
    if not Rituals.active then
        return
    end

    if Rituals.special == "normal" or Rituals.special == Rituals.hint then
        updateNormalRitual(dt)
    else
        updateSpecialRitual(dt)
    end
end

function updateNormalRitual(dt)
    Rituals.time = Rituals.time + dt
    if Rituals.time >= Rituals.delay then
        finishRitual()
        return
    end


    for _,item in ipairs(Rituals.vibrate) do
        item.ofs = Vector(math.random(5) - 3, math.random(3) - 2)
    end

    local hd = Rituals.delay/2
    local frac = 1 - math.abs(hd - Rituals.time)/hd
    for _,item in ipairs(Rituals.elevate) do
        item.ofs = Vector(math.random(7) - 4, math.random(3) - 2) + Vector(0, -200 * frac)
    end

    if Rituals.time < hd then
        Rituals.appear.opacity = 0
    else
        Rituals.appear.opacity = 1 - frac
    end
end

function updateSpecialRitual(dt)
    Rituals.time = Rituals.time + dt

    -- objs
    for _,item in ipairs(Rituals.vibrate) do
        item.ofs = Vector(math.random(5) - 3, math.random(3) - 2)
    end

    local hd = Rituals.delay/2
    local frac = 1 - math.max(0,(hd - Rituals.time)/hd)
    for _,item in ipairs(Rituals.elevate) do
        item.ofs = item.ofs + Vector(0, -200 * frac)
    end

    -- overlay
    if Rituals.time <= Rituals.delay*2 then
        local alp = Rituals.time / Rituals.delay * 0.5
        local rr = function() return (math.random() - 0.5) * 10 * alp end
        Rituals.overlayOp = alp
        screenShake = Vector( rr(), rr() )
    end

    if Rituals.time >= Rituals.delay*2 and not Rituals.special.visible then
        Rituals.special.visible = true
        screenShake = Vector(0,0)
        for _,item in ipairs(Rituals.elevate) do
            item.ofs = Vector(0,0)
        end
        Rituals.elevate = {}
        for _,item in ipairs(Rituals.vibrate) do
            item.ofs = Vector(0,0)
        end
        Rituals.vibrate = {}
        if Rituals.special == Rituals.devil then
            Rituals.distortion = true
        end

        -- music
        stopBgMusic()
        setBgMusicVol(1)
        playSound(Rituals.special.sound)
    end

    if Rituals.time > Rituals.delay*2 then
        local alp = math.max(0, math.min(0.5 * (Rituals.time - Rituals.delay*2.25), 1))
        Rituals.overlayOp = 1 - alp
    end

    if Rituals.special.visible then
        -- santa anim
        local R = Rituals.special
        local dirty = false
        R.time = R.time + dt
        while R.time > R.delay do
            R.time = R.time - R.delay
            R.frame = (R.frame % #R.quads) + 1
            dirty = true
        end

        if dirty then
            R.batch:clear()
            R.batch:setColor(255,255,255)
            local sc = R.scale or 1
            local ofs = R.ofs or Vector(0,0)
            R.batch:add(R.quads[R.frame], screenSize.x * 0.5 - 300 + ofs.x, 330 + ofs.y, 0, sc, sc)
        end

        if R.ps then
            R.ps:update(dt)
        end
    end

    -- distortion
    if Rituals.distortion then
        Rituals.distTime = (Rituals.distTime + dt) % (2 * math.pi)
        Rituals.distAmount = (math.cos(Rituals.distTime) + 1) * 0.5 * Rituals.distMax
    end

    -- sound
    if Rituals.time < Rituals.delay*2 then
        local alp = 1 - (Rituals.time / Rituals.delay * 0.5)
        setBgMusicVol(alp)
    end

    -- reset
    if Rituals.time > Rituals.delay*4 then
        readyToReset = true
        updateRitualButton(dt)
    end
end

-----------------------------------------------------------------------
-- FINISHING
function finishRitual()
    for _,item in ipairs(Rituals.elevate) do
        item.ofs = Vector(0,0)
    end
    for _,item in ipairs(Rituals.vibrate) do
        item.ofs = Vector(0,0)
    end
    Rituals.appear.opacity = 1

    Rituals.active = false
    recalcShines()
end

function updateRitualButton(dt)
    Rituals.buttonOp = increaseExponential(dt, Rituals.buttonOp, 0.95)

    if not fingers.active then
        local mpos = Vector(love.mouse.getX(), love.mouse.getY())
        mpos = (mpos - screenTopLeft) / screenAdjScale
        manageRitualMouse(mpos)
    end
end

function manageRitualMouse(mpos)
    Rituals.buttonHover = false
    if mpos.y < screenSize.y * 0.17 and mpos.x > screenSize.x * 0.79 then
        Rituals.buttonHover = true
    end
end

function ritualClicked(fpos)
    -- fingers
    if fpos then
        manageRitualMouse(fpos)
    end

    if Rituals.buttonHover then
        resetGame()
    end
end

-----------------------------------------------------------------------------------------
-- DRAW
function drawRituals()
    if not Rituals.active then
        return
    end

    if Rituals.special ~= "normal" then
        if Rituals.special.visible then
            -- red overlay
            if Rituals.special == Rituals.devil then
                setAdditiveMode()
                love.graphics.setColor(255,0,0, 96)
                love.graphics.rectangle("fill", 0, 0, screenSize.x, screenSize.y)
                love.graphics.setBlendMode("alpha")
            end


            love.graphics.setColor(255,255,255, (1-Rituals.overlayOp) * 255)
            love.graphics.draw(Rituals.special.batch)
            if Rituals.special.ps then
                love.graphics.draw(Rituals.special.ps)
            end
        end

        if Rituals.overlayOp > 0 then
            love.graphics.setColor(0,0,0, Rituals.overlayOp * 255)
            love.graphics.rectangle("fill", 0, 0, screenSize.x, screenSize.y)

            setAdditiveMode()
            love.graphics.setColor(255,0,0, Rituals.overlayOp*255)
            love.graphics.draw(Rituals.brightPenta, -screenShake.x, -screenShake.y)
            love.graphics.setBlendMode("alpha")
        end

        if readyToReset then
            love.graphics.setColor(0,0,0,128 * Rituals.buttonOp)
            love.graphics.rectangle("fill",screenSize.x * 0.79, screenSize.y * 0.04, screenSize.x * 0.15, screenSize.y * 0.13)

            love.graphics.setFont(buttonFont)
            if Rituals.buttonHover then
                love.graphics.setColor(255,128,128, 255 * Rituals.buttonOp)
            else
                love.graphics.setColor(255,0,0, 255 * Rituals.buttonOp)
            end
            love.graphics.print("Restart", screenSize.x * 0.8, screenSize.y * 0.05)
        end
    end
end
