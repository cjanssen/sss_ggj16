-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

local Box_proto = {
    copy = function(self) return Box(self.x0,self.y0,self.x1,self.y1) end,
    displace = function(box, vector) return Box(box.x0 + vector.x, box.y0 + vector.y, box.x1 + vector.x, box.y1 + vector.y) end,
    intersects = function(box1, box2) return box1.x1 >= box2.x0 and box1.x0 <= box2.x1 and box1.y0 <= box2.y1 and box1.y1 >= box2.y0 end,
    containsPoint = function(box, vector) return vector.x >= box.x0 and vector.x <= box.x1 and vector.y >= box.y0 and vector.y <= box.y1 end,
    topLeft = function(box) return Vector(box.x0, box.y0) end,
    topRight = function(box) return Vector(box.x1, box.y0) end,
    bottomRight = function(box) return Vector(box.x1, box.y1) end,
    bottomLeft = function(box) return Vector(box.x0, box.y1) end,
    width = function(box) return box.x1 - box.x0 end,
    height = function(box) return box.y1 - box.y0 end,
    size = function(box) return Vector(box:width(), box:height()) end,
    sortedCoords = function(box) return Box(math.min(box.x0, box.x1), math.min(box.y0, box.y1), math.max(box.x0, box.x1), math.max(box.y0, box.y1)) end,

    floor = function(self)
        return Box(math.floor(self.x0),math.floor(self.y0),math.floor(self.x1),math.floor(self.y1))
    end,
    unpack = function(self)
        return self.x0,self.y0,self.x1,self.y1
    end,
}

local Box_mt = {
        __index = function(table, key) return Box_proto[key] end,
        __add = function(lt,rt) return lt:displace(rt) end,
        __sub = function(lt,rt) return lt:displace(-rt) end,
    }


function Box(x0, y0, x1, y1, data)
    local box = { x0 = x0 or 0, y0 = y0 or 0, x1 = x1 or 0, y1 = y1 or 0 }
    if data and type(data) == "table" then
        for k,v in pairs(data) do box[k] = v end
    end
    setmetatable(box,Box_mt)
    return box
end

function BoxFromSquare(x,y,w,h)
    if type(x)=="table" then
        return Box(x.x,x.y,x.x+x.w,x.y+x.h)
    end
    return Box(x,y,x+w,y+h)
end

function BoxFromVectors(topleft, bottomright)
    return Box(topleft.x, topleft.y, bottomright.x, bottomright.y)
end

function CenteredBox(center, dimensions)
    return Box(center.x - dimensions.x*0.5, center.y - dimensions.y*0.5, center.x + dimensions.x*0.5, center.y + dimensions.y*0.5)
end