-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

function initItems()
    Items = {}
    Items.current = {}
    Items.selection = nil
    Items.snaps = {}
    Items.snapBox = Box(0,0,50,50)
    Items.floor = screenSize.y * 0.6
    Items.gravity = 5000
    Items.minScale = 0.666

    initPics()
end

function resetItems()
    Items.current = {}
    Items.selection = nil
    for _,itemName in ipairs(Combi.initialItems) do
        addItem(itemName)
    end

    addAllSnaps()
end

function addItem(itemName)
    local item = {
        foot = Vector(math.random()*(screenSize.x - 160), math.random()*(screenSize.y - 160)),
        size = Vector(160,160),
        scale = Items.minScale,
        getBox = function(self)
            local sz = self.size * self.scale
            return Box(self.foot.x - sz.x * 0.5, self.foot.y - sz.y, self.foot.x + sz.x * 0.5, self.foot.y)
        end,
        acc = Vector(0,0),
        vel = Vector(0,0),
        --color = {(math.random()*0.5+0.25)*255, (math.random()*0.5+0.25)*255, (math.random()*0.5+0.25)*255},
        name = itemName,
        opacity = 1,
        ofs = Vector(0,0),
        shineOpacity = 1,
        }
    -- special: Balloon
    if item.name == "Balloon" then
        item.floatOfs = Vector(0,0)
        item.t = 0
    end
    table.insert(Items.current, item)
    return item
end


function removeItem(itemName)
    for i,item in ipairs(Items.current) do
        if item.name == itemName then
            if item.hasSnap then
                item.hasSnap.hasItem = nil
            end
            table.remove(Items.current, i)
            break
        end
    end
end

function recreateItem(itemName)
    local newItem = addItem(itemName)
    local availableShelves = {}
    for _,snap in ipairs(Items.snaps) do
        if not snap.isSpecial and not snap.hasItem then
            table.insert(availableShelves, snap)
        end
    end

    putInSnap(newItem, availableShelves[math.random(#availableShelves)])
    return newItem
end

function repositionItemsInShelves()
    local availableShelves = {}
    for _,snap in ipairs(Items.snaps) do
        if not snap.isSpecial then table.insert(availableShelves, snap) end
    end
    for _,item in ipairs(Items.current) do
        local snap = table.remove(availableShelves, math.random(#availableShelves))
        putInSnap(item, snap)
    end
end

function updateItems(dt)
    local itemMoved = false
    for _,item in ipairs(Items.current) do
        local oldPosY = item.foot.y
        updateItem(item, dt)
        if not itemMoved and item.foot.y ~= oldPosY then
            itemMoved = true
        end
    end

    -- prevent flicking when z-sorting
    if itemMoved then
        local posDict = {}
        for i,item in ipairs(Items.current) do
            if posDict[item.foot.y] then
                item.foot.y = item.foot.y + i/#Items.current
            end
            posDict[item.foot.y] = true
        end
        sortItems()
    end


    -- mouse
    if not Rituals.active then
        if not fingers.active then
            local mpos = Vector(love.mouse.getX(), love.mouse.getY())
            mpos = (mpos - screenTopLeft) / screenAdjScale
            updateSelection(mpos, isMouseDown())
        else
            local mpos,isDown = manageFingers()
            updateSelection(mpos, isDown)
        end
    end
end



function updateSelection(mpos,isDown)
    if not Items.selection and isDown then
        local item = getItemUnder(mpos)
        if item then
            takeItem(item, mpos)
        end
    elseif Items.selection and not isDown then
        releaseItem(Items.selection.target)
        Items.selection = nil
    elseif isDown then
        Items.selection.target.foot = mpos + Items.selection.opos
    end
end

function takeItem(item, mpos)
    Items.selection = {
        opos = item.foot - mpos,
        target = item
    }

    -- reset movement
    item.acc = Vector(0,0)
    item.vel = Vector(0,0)

    -- playSound(Sfx.pick)
end


function releaseItem(item)
    local function itemInSnap(snap)
        return snap.box:intersects(item:getBox())
    end
    local function getSnap()
        for _,snap in ipairs(Items.snaps) do
            if itemInSnap(snap) and (not snap.hasItem or snap.hasItem == item) then return snap end
        end
    end

    keepItemInBounds(item)

    local snap = getSnap()
    if snap then
        -- snap into position
        putInSnap(item, snap)
        if snap.isSpecial == "pentagram" then
            playSound(Sfx.place)
        end

        if snap.isSpecial == "pentagram" and not Items.centralSnap.hasItem then
            local res = checkInvocation()
            if res then
                if not manageSpecialActivation(res) then
                    local newItem = findItem(res)
                    if not newItem then
                        newItem = addItem(res)
                        putInSnap(newItem, Items.centralSnap)
                        appearRitual(newItem)
                        launchRitual()
                    end
                end

                -- if not newItem.hasSnap or newItem.hasSnap.isSpecial ~= "pentagram" then

                -- end
            end
        end

        if snap.isSpecial == "burninate" then
            burninate(item)
        end

    elseif item.hasSnap then
        -- fall down
        dropFromSnap(item)
    end
end

function keepItemInBounds(item)
    local box = item:getBox()
    if box.x0 < 0 then
        item.foot.x = item.foot.x - box.x0
    elseif box.x1 > screenSize.x then
        item.foot.x = item.foot.x - (box.x1 - screenSize.x)
    end

    if box.y0 < 0 then
        item.foot.y = item.foot.y - box.y1
    elseif box.y1 > screenSize.y then
        item.foot.y = item.foot.y - (box.y1 - screenSize.y)
    end
end

function manageSpecialActivation(itemName)
    if itemName == "Santa" then
        launchSantaRitual()
        return true
    elseif itemName == "Goat" then
        launchGoatRitual()
        return true
    elseif itemName == "Satan" then
        launchDevilRitual()
        return true
    end

    return false
end

function findItem(itemName)
    return findInList(Items.current, function(item) return item.name == itemName end)
end

function dropFromSnap(item)
    item.hasSnap.hasItem = nil
    item.hasSnap = nil
end

function putInSnap(item, snap)
    item.foot = snap.foot:copy()

    if item.hasSnap then
        item.hasSnap.hasItem = nil
    end
    item.hasSnap = snap
    snap.hasItem = item
end

function isItemOnFloor(item)
    return item.foot.y >= Items.floor
end

function getItemUnder(mpos)
    for _,item in ipairs(Items.current) do
        local npos = (mpos - item:getBox():topLeft()) / item.scale
        if npos.x >= 0 and npos.y >= 0 and npos.x <= item.size.x and npos.y <= item.size.y then
            return item
        end
    end
    return nil
end



function sortItems()
    table.sort(Items.current, function(a,b) return a.foot.y < b.foot.y end)
end

function updateItem(item, dt)
    -- fall
    if not item.hasSnap and not isItemOnFloor(item) and (not Items.selection or Items.selection.target ~= item) and item.name ~= "Balloon" then
        item.acc.y = Items.gravity
        item.vel.y = item.vel.y + item.acc.y * dt
        item.foot.y = item.foot.y + item.vel.y * dt
    else
        if item.vel.y > 0 then
            item.foot.y = Items.floor
            playSound(Sfx.fall)
        end
        item.vel.y = 0
        item.acc.y = 0
    end

    -- shine
    local shine = checkShine(item.name)
    if shine and item.shineOpacity < 1 then
        item.shineOpacity = increaseExponential(dt, item.shineOpacity, 0.95)
    elseif not shine and item.shineOpacity > 0 then
        item.shineOpacity = decreaseExponential(dt, item.shineOpacity, 0.95)
    end

    -- perspective
    if item.foot.y < Items.floor then
        item.scale = Items.minScale
    else
        item.scale = (1-Items.minScale) * (item.foot.y - Items.floor) / (screenSize.y - Items.floor) + Items.minScale
    end

    -- balloon
    if item.name == "Balloon" then
        item.t = (item.t + dt) or 0
        item.floatOfs.y = 20 * math.sin(item.t)
        item.floatOfs.x = 10 * math.sin(item.t * math.sqrt(2)/2)
    end

    sortItems()
end

function checkInvocation()
    local invoItems = {}
    for _,penta in ipairs(Items.pentaSnaps) do
        if not penta.hasItem then
            break
        end
        table.insert(invoItems, penta.hasItem.name)
    end

    if #Items.pentaSnaps == #invoItems then
        local res = getCombiResult(invoItems)
        if res then
            local isNew = consumeCombi(res)
            if isNew then
                combineRitual(res[1])
                vibrateRitual(invoItems)
            end
            return res[2]
        else
            checkNearMiss(invoItems)
            return nil
        end
    end

    return nil
end

function checkNearMiss(nameList)
    local candidates = getNearMiss(nameList)
    if candidates and #candidates > 2 then
        combineRitual({})
        vibrateRitual(candidates)
        appearRitual({})
        launchHintRitual()
    end
end


function drawItems()
    love.graphics.setColor(255,255,255)
    love.graphics.draw(Items.shadowBatch, 0, 30)
    love.graphics.draw(Items.batch, 0, 30)
end