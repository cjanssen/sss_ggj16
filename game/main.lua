-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

require 'vector'
require 'box'

function love.load()
    love.filesystem.load("util.lua")()
    love.filesystem.load("pictures.lua")()
    love.filesystem.load("items.lua")()
    love.filesystem.load("combi.lua")()
    love.filesystem.load("snaps.lua")()
    love.filesystem.load("rituals.lua")()
    love.filesystem.load("burn.lua")()
    love.filesystem.load("background.lua")()
    love.filesystem.load("music.lua")()
    love.filesystem.load("intro.lua")()
    love.filesystem.load("touch_wrapper.lua")()

    initGame()
end

function initGame()
    gameIcon = love.graphics.newImage("img/logo/icon.png"):getData()

    screenSize = Vector(1920, 1080)
    love.window.setMode(screenSize.x, screenSize.y, {fullscreen = true, fullscreentype = "desktop"})
    postModeChange()
    screenTopLeft = Vector(0,0)
    screenAdjScale = 1


    initBackground()
    initCombi()
    initSnaps()
    initItems()
    initMusic()
    initSfx()
    initRituals()
    initIntro()
    initBurn()

    buttonFont = love.graphics.newFont("fnt/SheDevil.ttf",100)
    textFont = love.graphics.newFont("fnt/SheDevil.ttf",72)

    resetGame()
end

function postModeChange()
    love.window.setTitle( "Satan Summoning Simulator")
    love.window.setIcon(gameIcon)
end

function resetGame()
    resetPics()
    resetCombi()
    resetItems()
    resetRituals()
    resetMusic()
    resetIntro()
    resetBurn()

    readyToReset = false
    screenShake = Vector(0,0)
end

function love.update(dt)
    updateIntro(dt)
    updateItems(dt)
    updateRituals(dt)
    updatePics(dt)
    updateBurn(dt)
    updateBackground(dt)
end

function love.draw()
    -- out of bounds
    love.graphics.setColor(0,0,0)
    love.graphics.setScissor()
    love.graphics.rectangle("fill",0,0,love.graphics.getWidth(), love.graphics.getHeight())

    love.graphics.push()

    local ssw,ssh = love.graphics.getWidth(), love.graphics.getHeight()
    local sc = math.min(ssw/screenSize.x, ssh/screenSize.y)
    local sw,sh = screenSize.x*sc, screenSize.y*sc
    local ox = ssw>sw and (ssw-sw)*0.5 or 0
    local oy = ssh>sh and (ssh-sh)*0.5 or 0

    local realRes = Vector(love.graphics.getWidth(), love.graphics.getHeight())
    screenTopLeft = Vector(ox,oy)
    screenAdjScale = sc

    love.graphics.setScissor(ox,oy,sw,sh)

    love.graphics.translate(ox,oy)
    love.graphics.scale(sc,sc)

    drawIter()
    love.graphics.pop()
end

function drawIter()
    love.graphics.push()

    love.graphics.translate(screenShake.x, screenShake.y)

    if Rituals.distortion then
        love.graphics.push()
        love.graphics.translate(screenSize.x * 0.5, screenSize.y * 0.5)
        -- local sc = (1 + math.cos(t)) * 0.1
        love.graphics.scale(1 + Rituals.distAmount, 1 + Rituals.distMax - Rituals.distAmount)
        love.graphics.translate(-screenSize.x * 0.5, -screenSize.y * 0.5)
    end

    drawBackground()
    drawItems()
    drawForeground()

    if Rituals.distortion then
        love.graphics.pop()
    end
    drawRituals()

    drawIntro()

    love.graphics.pop()
end


function love.mousepressed(x,y)
    if readyToReset then
        ritualClicked()
    end

    if Intro.active then
        introClicked()
    end
end


function love.keypressed(key)
    if key == "escape" then
        love.event.push("quit")
        return
    end

    if key == "f1" then
        local fs = love.window.getFullscreen()
        love.window.setMode(screenSize.x, screenSize.y, {fullscreen = not fs, fullscreentype = "desktop", resizable = true})
        postModeChange()
        return
    end

    if key == "f5" then
        resetGame()
        return
    end

    if readyToReset then
        ritualClicked()
    end

    if Intro.active then
        introClicked()
    end
end