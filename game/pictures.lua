-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

function initPics()
    loadItemImages()
    initAnims()
end

function loadItemImages()
    local img = love.graphics.newImage("img/obj/Items.png")
    local shadowImg = love.graphics.newImage("img/obj/ShadowItems.png")
    Items.quads = {}
    Items.batch = love.graphics.newSpriteBatch(img, 1000)
    Items.shadowBatch = love.graphics.newSpriteBatch(shadowImg, 1000)
    Background.candleBatch = love.graphics.newSpriteBatch(img, 20)
    Items.animMap = {}

    local spriteData = getSpriteData()
    for i,elem in ipairs(spriteData) do
        Items.quads[i] = love.graphics.newQuad(elem.frame.x, elem.frame.y + 20, elem.frame.w, elem.frame.h, img:getWidth(), img:getHeight())
        local en = filenameToItemName(elem.filename)
        if en then
            if not Items.animMap[en] then Items.animMap[en] = { w = elem.frame.w, h = elem.frame.h } end
            table.insert(Items.animMap[en], i)
        end
    end
end

function initAnims()
    Items.animDelay = 0.15

    Items.allAnims = {}
    for _,anim in pairs(Items.animMap) do
        if #anim>1 then
            anim.current = 1
            anim.time = 0
            table.insert(Items.allAnims, anim)
        end
    end
end

function resetPics()
    for _,anim in ipairs(Items.allAnims) do
        anim.current = 1
        anim.time = 0
    end
end

function updatePics(dt)
    Items.batch:clear()
    Items.shadowBatch:clear()

    for _,anim in ipairs(Items.allAnims) do
        anim.time = anim.time + dt
        while anim.time > Items.animDelay do
            anim.time = anim.time - Items.animDelay
            anim.current = (anim.current % #anim) + 1
        end
    end

    for _,item in ipairs(Items.current) do
        if Items.animMap[item.name] then
            local elem = Items.animMap[item.name]
            local ofs = item.ofs
            -- special animation for the balloon
            if item.name == "Balloon" then
                ofs = ofs + item.floatOfs
            end

            if item.shineOpacity > 0 then
                local shine = checkShine(item.name)
                if shine == 1 then
                    Items.shadowBatch:setColor(89, 159, 255, item.opacity * item.shineOpacity * 255)
                else
                    Items.shadowBatch:setColor(210, 0, 220, item.opacity * item.shineOpacity * 255)
                end
                Items.shadowBatch:add(getSpriteForObject(item.name), item.foot.x + ofs.x, item.foot.y + ofs.y, 0, item.scale, item.scale, elem.w * 0.5, elem.h)
            end

            local r,g,b = 255,255,255
            if item.color then
                r,g,b = item.color[1],item.color[2],item.color[3]
            end
            Items.batch:setColor(r,g,b, item.opacity * 255)

            Items.batch:add(getSpriteForObject(item.name), item.foot.x + ofs.x, item.foot.y + ofs.y, 0, item.scale, item.scale, elem.w * 0.5, elem.h)
        end
    end
end


function getSpriteForObject(en)
    if false then
        local an = Items.animMap[en]
        return Items.quads[an[1]]
    end

    local function sp()
        local an = Items.animMap[en]
        if #an == 1 then return an[1] end
        return an[an.current]
    end
    return Items.quads[sp()]
end

local spriteData = {
    {filename = "BeerBottle/0022",
    frame= {x=1,y=1,w=320,h=320}},
    {filename = "Blood/0003",
    frame= {x=1,y=323,w=320,h=320}},
    {filename = "BloodCandle/0012",
    frame= {x=1,y=645,w=320,h=320}},
    {filename = "BloodCandle/0013",
    frame= {x=1,y=967,w=320,h=320}},
    {filename = "BloodCandle/0014",
    frame= {x=1,y=1289,w=320,h=320}},
    {filename = "Bones/0011",
    frame= {x=1,y=1611,w=320,h=320}},
    {filename = "Book/0018",
    frame= {x=323,y=1,w=320,h=320}},
    {filename = "CanOfSoup/0010",
    frame= {x=645,y=1,w=320,h=320}},
    {filename = "Candle/0024",
    frame= {x=967,y=1,w=320,h=320}},
    {filename = "Candle/0025",
    frame= {x=1289,y=1,w=320,h=320}},
    {filename = "Candle/0026",
    frame= {x=1611,y=1,w=320,h=320}},
    {filename = "DrinkingHorn/0004",
    frame= {x=323,y=323,w=320,h=320}},
    {filename = "Flame/0027",
    frame= {x=323,y=645,w=320,h=320}},
    {filename = "Flame/0028",
    frame= {x=323,y=967,w=320,h=320}},
    {filename = "Flame/0029",
    frame= {x=323,y=1289,w=320,h=320}},
    {filename = "SatanicBible/0015",
    frame= {x=323,y=1611,w=320,h=320}},
    {filename = "SatanicBible/0016",
    frame= {x=645,y=323,w=320,h=320}},
    {filename = "SatanicBible/0017",
    frame= {x=967,y=323,w=320,h=320}},
    {filename = "Steak/0021",
    frame= {x=1289,y=323,w=320,h=320}},
    {filename = "Tomato/0020",
    frame= {x=1611,y=323,w=320,h=320}},
    {filename = "VikingHelmet/0019",
    frame= {x=645,y=645,w=320,h=320}},
    {filename = "WineBottle/0023",
    frame= {x=645,y=967,w=320,h=320}},
    {filename = "Knife",
    frame= {x=967,y=1289,w=320,h=320}},
    {filename = "Pocket Watch",
    frame= {x=967,y=967,w=320,h=320}},
    {filename = "Stick",
    frame= {x=967,y=645,w=320,h=320}},
    {filename = "Mjollnir",
    frame= {x=645,y=1289,w=320,h=320}},
    {filename = "DirtyCash",
    frame= {x=645,y=1611,w=320,h=320}},
    {filename = "DogFood",
    frame= {x=967,y=1611,w=320,h=320}},
    {filename = "Puppy_1",
    frame= {x=1289,y=645,w=320,h=320}},
    {filename = "Puppy_2",
    frame= {x=1289,y=967,w=320,h=320}},
    {filename = "Puppy_3",
    frame= {x=1289,y=1289,w=320,h=320}},
    {filename = "Puppy_4",
    frame= {x=1289,y=1611,w=320,h=320}},
    {filename = "Plant",
    frame= {x=1611,y=645,w=320,h=320}},
    {filename = "Sputnik_1",
    frame= {x=1611,y=967,w=320,h=320}},
    {filename = "Sputnik_2",
    frame= {x=1611,y=1289,w=320,h=320}},
    {filename = "Goat_1",
    frame= {x=1933,y=1,w=320,h=320}},
    {filename = "Goat_2",
    frame= {x=1933,y=323,w=320,h=320}},
    {filename = "Goat_3",
    frame= {x=1933,y=645,w=320,h=320}},
    {filename = "Battery_1",
    frame= {x=1933,y=967,w=320,h=320}},
    {filename = "Battery_2",
    frame= {x=1933,y=1289,w=320,h=320}},
    {filename = "Battery_3",
    frame= {x=1933,y=1611,w=320,h=320}},
    {filename = "EnergyDrink_1",
    frame= {x=1,y=1933,w=320,h=320}},
    {filename = "EnergyDrink_2",
    frame= {x=321,y=1933,w=320,h=320}},
    {filename = "EnergyDrink_3",
    frame= {x=641,y=1933,w=320,h=320}},
    {filename = "TinTinRocket",
    frame= {x=1611,y=1611,w=320,h=320}},
    {filename = "Balloon_1",
    frame= {x=967,y=1933,w=320,h=320}},
    {filename = "Balloon_2",
    frame= {x=1287,y=1933,w=320,h=320}},
    {filename = "Balloon_3",
    frame= {x=1607,y=1933,w=320,h=320}},
}

local fnameToItemName = {
    ["Blood/0003"] = "Blood",
    ["BloodCandle/0012"] = "Blood Candle",
    ["BloodCandle/0013"] = "Blood Candle",
    ["BloodCandle/0014"] = "Blood Candle",
    ["Bones/0011"] = "Bones",
    ["Book/0018"] = "Book",
    ["CanOfSoup/0010"] = "Can of Soup",
    ["DrinkingHorn/0004"] = "Drinking Horn",
    ["SatanicBible/0015"] = "Satanic Bible",
    ["SatanicBible/0016"] = "Satanic Bible",
    ["SatanicBible/0017"] = "Satanic Bible",
    ["BeerBottle/0022"] = "Beer Bottle",
    ["Candle/0024"] = "Candle",
    ["Candle/0025"] = "Candle",
    ["Candle/0026"] = "Candle",
    ["Flame/0027"] = "Flame",
    ["Flame/0028"] = "Flame",
    ["Flame/0029"] = "Flame",
    ["Steak/0021"] = "Steak",
    ["Tomato/0020"] = "Tomato",
    ["VikingHelmet/0019"] = "Viking Helmet",
    ["WineBottle/0023"] = "Winebottle",
    ["Knife"] = "Knife",
    ["Pocket Watch"] = "Pocket Watch",
    ["Stick"] = "Stick",
    ["Mjollnir"] = "Hammer",
    ["DirtyCash"] = "Money",
    ["DogFood"] = "Dog Food",
    ["Plant"] = "Pot Flower",
    ["Puppy_1"] = "Puppy",
    ["Puppy_2"] = "Puppy",
    ["Puppy_3"] = "Puppy",
    ["Puppy_4"] = "Puppy",
    ["Sputnik_1"] = "Satellite",
    ["Sputnik_2"] = "Satellite",
    ["Goat_1"] = "Goat",
    ["Goat_2"] = "Goat",
    ["Goat_3"] = "Goat",
    ["Battery_1"] = "Battery",
    ["Battery_2"] = "Battery",
    ["Battery_3"] = "Battery",
    ["EnergyDrink_1"] = "Energydrink",
    ["EnergyDrink_2"] = "Energydrink",
    ["EnergyDrink_3"] = "Energydrink",
    ["TinTinRocket"] = "Rocket",
    ["Balloon_1"] = "Balloon",
    ["Balloon_2"] = "Balloon",
    ["Balloon_3"] = "Balloon",
}

function getSpriteData()
    return spriteData
end

function filenameToItemName(filename)
    return fnameToItemName[filename]
end
