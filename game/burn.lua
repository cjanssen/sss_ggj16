-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

function initBurn()
    Burn = {}
    Burn.delay = 3
end

function resetBurn()
    Burn.items = {}
    Burn.newItems = {}
end

function burninate(item)
    table.insert(Burn.items,item)
    item.time = 0
    removeItem(item.name)
    playSound(Sfx.burn)
end

function updateBurn(dt)
    for i = #Burn.items,1,-1 do
        local item = Burn.items[i]
        item.time = item.time + dt

        local c = math.clamp(0, 1 - item.time / Burn.delay / 0.75, 1)
        item.color = {255*c,255*c,255*c}
        item.opacity = math.clamp(0,c*3,1)

        if item.time >= Burn.delay * 0.75 then
            local newItem = recreateItem(item.name)
            newItem.opacity = 0
            newItem.time = item.time - dt
            table.remove(Burn.items,i)
            table.insert(Burn.newItems, newItem)
        end
    end

    for i = #Burn.newItems,1,-1 do
        local newItem = Burn.newItems[i]
        newItem.time = newItem.time + dt
        newItem.opacity = math.clamp(0,(newItem.time - Rituals.delay * 0.75) / (Rituals.delay * 0.25),1)
        if newItem.time >= Rituals.delay then
            table.remove(Burn.newItems, i)
        end
    end
end

function drawBurn()
    for _,item in ipairs(Burn.items) do
        local op = item.opacity
        local sprite = getSpriteForObject(item.name)

        if item.shineOpacity > 0 then
            local shine = checkShine(item.name)
            if shine == 1 then
                love.graphics.setColor(89, 159, 255, op * item.shineOpacity * 255)
            else
                love.graphics.setColor(210, 0, 220, op * item.shineOpacity * 255)
            end
            love.graphics.draw(getBatchImage(Items.shadowBatch),sprite, item.foot.x + item.ofs.x, item.foot.y + item.ofs.y, 0, item.scale, item.scale, 160, 320)
        end

        local r,g,b = item.color[1],item.color[2],item.color[3]
        love.graphics.setColor(r,g,b, op * 255)
        love.graphics.draw(getBatchImage(Items.batch), sprite, item.foot.x + item.ofs.x, item.foot.y + item.ofs.y, 0, item.scale, item.scale, 160, 320)

        love.graphics.setColor(255,255,255)
    end
end