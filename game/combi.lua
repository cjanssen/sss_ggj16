-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

function initCombi()
    Combi = {}

    Combi.items = {
        "Blood Candle",
        "Blood",
        "Satanic Bible",
        "Bones",
        "Drinking Horn",
        "Pot Flower",
        "Pocket Watch",
        "Dog Food",
        "Steak",
        "Beer Bottle",
        "Stick",
        "Book",
        "Puppy",
        "Viking Helmet",
        "Knife",
        "Can of Soup",
        "Tomato",
        "Balloon",
        "Winebottle",
        "Hammer",
        "Battery",
        "Satellite",
        "Money",
        "Candle",
        "Energydrink",
        "Rocket"
    }

    Combi.pairs = {
        {{"Steak","Puppy"},"Bones"},
        {{"Viking Helmet","Hammer"},"Drinking Horn"},
        {{"Puppy","Knife"},"Blood"},
        {{"Candle","Blood"},"Blood Candle"},
        {{"Book","Satellite"},"Satanic Bible"},
        {{"Pot Flower","Pocket Watch"},"Stick"},
        {{"Stick","Pocket Watch"},"Book"},
        {{"Dog Food","Stick"},"Puppy"},
        {{"Beer Bottle","Bones"},"Viking Helmet"},
        {{"Viking Helmet","Steak"},"Knife"},
        {{"Dog Food","Blood"},"Can of Soup"},
        {{"Can of Soup","Pot Flower"},"Tomato"},
        {{"Bones","Pot Flower"},"Balloon"},
        {{"Balloon","Pocket Watch"},"Winebottle"},
        {{"Winebottle","Stick"},"Hammer"},
        {{"Dog Food","Puppy"},"Battery"},
        {{"Balloon","Battery"},"Satellite"},
        {{"Puppy","Satellite"},"Money"},
        {{"Stick","Money"},"Candle"},
        {{"Battery","Beer Bottle"},"Energydrink"},
        {{"Energydrink","Satellite"},"Rocket"},
        }

    Combi.finalPairs = {
        {{"Blood Candle", "Drinking Horn", "Bones", "Blood", "Satanic Bible"}, "Satan"},
        {{"Puppy","Pot Flower","Balloon","Drinking Horn","Tomato"},"Goat"},
        {{"Rocket","Tomato","Battery","Balloon","Candle"},"Santa"},
    }

    Combi.initialItems = {
        "Pot Flower",
        "Pocket Watch",
        "Dog Food",
        "Steak",
        "Beer Bottle",
        "Viking Helmet",
    }

    resetCombi()
end

function resetCombi()
    Combi.missingPairs = randomizedListCopy(Combi.pairs)
    Combi.donePairs = {}

    Combi.availableItems = cloneListRecur(Combi.initialItems)

    recalcShines()
end

function checkShine(elemName)
    return Combi.shines[elemName]
end


function recalcShines()
    Combi.shines = {}

    local itemMap = {}
    for _,itemName in ipairs(Combi.availableItems) do
        itemMap[itemName] = true
    end


    for _,group in ipairs({Combi.missingPairs, Combi.finalPairs}) do
        for _,pair in ipairs(group) do
            local possible = true
            for _,ingredient in ipairs(pair[1]) do
                if not itemMap[ingredient] then
                    possible = false
                    break
                end
            end

            if possible then
                for _,ingredient in ipairs(pair[1]) do
                    local ms = Combi.shines[ingredient] or 2
                    local Ms = (group == Combi.missingPairs and 1 or 2)
                    Combi.shines[ingredient] = math.min(ms, Ms)
                end
            end
        end
    end
end


function getCombiResult(itemList)
    for _,group in ipairs({Combi.missingPairs, Combi.finalPairs}) do
        for _,pair in ipairs(group) do
            local itemMap = {}
            for _,item in ipairs(itemList) do
                itemMap[item] = true
            end

            local matches = 0
            for _,refItem in ipairs(pair[1]) do
                if itemMap[refItem] then
                    matches = matches + 1
                end
            end

            if matches == #pair[1] then
                return pair
            end
        end
    end
end

function consumeCombi(refPair)
    local isNew = false
    for i,pair in ipairs(Combi.missingPairs) do
        if pair == refPair then
            table.remove(Combi.missingPairs, i)
            table.insert(Combi.donePairs, refPair)
            table.insert(Combi.availableItems, pair[2])
            isNew = true
            break
        end
    end

    -- randomize output each time to prevent boring repetition
    Combi.donePairs = randomizedListCopy(Combi.donePairs)

    return isNew
end


function getNearMiss(nameList)
    local results = {}
    for _,pair in ipairs(Combi.finalPairs) do
        -- prepare map of matches
        local matches = {}
        for _,name in ipairs(nameList) do
            matches[name] = true
        end

        local res = {}
        -- mark the matches
        for _,dest in ipairs(pair[1]) do
            if matches[dest] then
                table.insert(res,dest)
            end
        end

        if #res > 0 then
            table.insert(results, res)
        end
    end

    if #results == 0 then return nil end
    table.sort(results, function(a,b) return #a > #b end)
    return results[1]
end
