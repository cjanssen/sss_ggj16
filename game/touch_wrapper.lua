-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

fingers = {
    pos = Vector(0,0)
}

function love.touchpressed(id,x,y)
    fingers.active = true
    registerTouch(id,x,y,true)

    manageFingerPress(x,y)
end

function manageFingerPress(x,y)
    local fpos = Vector(x,y) ^ screenSize
    if readyToReset then
        ritualClicked(fpos)
    end

    if Intro.active then
        introClicked(fpos)
    end
end

function love.touchreleased(id,x,y)
    registerTouch(id,x,y,false)
end

function love.touchmoved(id,x,y)
    registerTouch(id,x,y,true)
end

function registerTouch(id,x,y,touch)
    local pos = Vector(x,y) ^ screenSize

    if not fingers.id and getItemUnder(pos) then
        fingers.id = id
    end

    if fingers.id == id then
        fingers.pos = pos
        fingers.touch = touch
    end

    if not fingers.touch then
        fingers.id = nil
    end
end

function manageFingers()
    return fingers.pos,fingers.touch
end
