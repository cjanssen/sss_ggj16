-- Satan Summoning Simulator V1.0
-- Copyright (C) 2015 Christiaan Janssen, Kirill Krysov, Alpha Rats, Arisa Matzanke, Cedric Douhaire, Ruth Bosch
-- Licensed under: CC-NC-BY-SA 3.0 (see License.txt)

function initMusic()
    SoundOn = true

    Music = {
        love.audio.newSource("music/Fire.ogg", "stream"),
        love.audio.newSource("music/bass.ogg", "stream"),
        love.audio.newSource("music/drony.ogg", "stream"),
        devil = love.audio.newSource("music/Satan.ogg", "stream"),
        santa = love.audio.newSource("music/Santa.ogg", "stream"),
    }

    for _,music in ipairs(Music) do
        music:setLooping(true)
    end

    -- stop all currently playing music if any
    love.audio.stop()
end

function initSfx()
    Sfx = {
        fall = love.audio.newSource("sfx/GroundHit_1.ogg"),
        place = love.audio.newSource("sfx/GroundHit_2.ogg"),
        magic = love.audio.newSource("sfx/Magic.ogg"),
        magic2 = love.audio.newSource("sfx/Magic2.ogg"),
        goat = love.audio.newSource("sfx/goat.ogg"),
        laugh = love.audio.newSource("sfx/Laugh.ogg"),
        burn = love.audio.newSource("sfx/burn.ogg")
    }
end

function resetMusic()
    for _,music in ipairs(Music) do
        music:stop()
        if SoundOn then
            music:play()
        end
    end
end

function stopBgMusic()
    for _,music in ipairs(Music) do
        music:stop()
    end
end

function playSound(snd)
    if SoundOn then
        snd:clone():play()
    end
end

function setBgMusicVol(vol)
    for _,music in ipairs(Music) do
        music:setVolume(vol)
    end
end

