SSS : Satan Summoning Simulator
-------------------------------

Made at the Global Game Jam Berlin 2016.

Combine items to get new items.  Finally, you can summon Satan.  The game has 3 different endings.


Credits
-------

    Concept and Game Design: Arisa Matzanke
    Art: Alpha Rats + Kirill Krysov + Ruth Bosch
    Music: AXL OTR
    Code: Christiaan Janssen
    Game Engine: Löve2D


Installation Instructions
-------------------------

    1. Unpack the zipfile for your platform.
    2. Run the executable you will find inside.


Special keys
------------

The game is controlled with the mouse.  During the game, you can use these special keys at any time:

    F1 - Switch fullscreen
    F5 - Reset game
    ESC - Exit game


License
-------

    Creative Commons 3.0 NonCommercial Attribution Share-alike (CC-NC-BY-SA)


Third party content
-------------------
   
Sounds from freesound.org by the following users:
    
    reinsamba, RICHERlandTV, toiletrolltube, Augdog, Speedenza, Leady
    
Font:

    She Devil by Swimmingbell




Berlin, January 2016
